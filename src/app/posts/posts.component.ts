import { UsersService } from './../users.service';
import { PostsService } from './../posts.service';
import { Post } from './../interface/post';
import { Component, OnInit } from '@angular/core';
import { Users } from '../interface/user';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  Posts$:Observable<any>;
  Users$:Users[];
  title:string;
  author:string;
  userId:string;
  constructor(private postsService:PostsService, 
            private userService:UsersService,  
            private router:Router,private route:ActivatedRoute, 
            public authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.Posts$ = this.postsService.getPosts(this.userId);        }
    )

  }
/* onSubmit(){
    for(let i = 0; i<this.Posts$.length;i++){
      for(let j = 0; j<this.Users$.length;j++){
         if(this.Posts$[i].userId == this.Users$[j].id) 
          this.postsService.savePosts(this.Posts$[i].title,this.Posts$[i].body,this.Users$[j].name);
      }
    }
  }*/
  
  deletePost(id:string){
    this.postsService.deletePost(id, this.userId);
  }

}
