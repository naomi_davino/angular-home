import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  author:string;
  id: number;
  authorEdit = new FormControl('', [Validators.required]);


  constructor(private router: Router, private route:ActivatedRoute) { }

  ngOnInit() {
    this.authorEdit = this.route.snapshot.params['author'];
    this.id = this.route.snapshot.params['id'];
  }

  onSubmit(){
    this.router.navigate(['/authors', this.id, this.authorEdit]);
  }

}
