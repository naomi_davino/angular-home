export interface User {
    uid:string,
    email?: string | null,
    photoUrl?: string,
    displayName?:string
 }
 
 export interface UserPost {

    id: number ,
    name: string,
    username: string,
    email: string,
    address: {
      street: string,
      suite: string,
      city: string,
      zipcode: number,
      geo: {
        lat: number,
        lng: number
      }
    },
    phone: number,
    website: string,
    company: {
      name: string,
      catchPhrase: string,
      bs: string
    }
  
 }

 export interface Users {
  id:number,
  name:string
}
