import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Books } from './interface/books';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient, private db: AngularFirestore) { }
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;

  getBooks(userId: string): Observable<any[]> {
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    
    return this.bookCollection.snapshotChanges().pipe(
      //pipe מאפשר לשרשר פונקציות
      map(
        //map גם מאפשר לשרשר פונקציות
        collection => collection.map(
          document => {
            debugger;
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            console.log(data);
            return data;
          }
        )
      )
    )
  }

  getBook(userId: string, id: string): Observable<any> {
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }
  addBook(userId: string, title: string, author: string) {
    const book: Books = { title: title, author: author };
    this.userCollection.doc(userId).collection('books').add(book);
  }
  updateBook(userId: string, id: string, title: string, author: string) {
    const book: Books = { title: title, author: author };
    this.db.doc(`users/${userId}/books/${id}`).update(book);
  }
  deleteBook(id: string, userId: string) {
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }
}
