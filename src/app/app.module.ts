import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { RouterModule, Routes } from '@angular/router';
import { AuthorsComponent } from './authors/authors.component';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { SingUpComponent } from './sing-up/sing-up.component';
import { FireStorePostsComponent } from './fire-store-posts/fire-store-posts.component';
import { AddPostComponent } from './add-post/add-post.component';
import { LoginComponent } from './login/login.component';
import { AddBookComponent } from './add-book/add-book.component';




const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'addBook', component: AddBookComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'authors/:id/:author', component: AuthorsComponent },
  { path: 'authors/:id/:author/edit', component: EditauthorComponent },
  { path: 'signup', component: SingUpComponent },
  { path: 'login', component: LoginComponent },
  { path: 'Posts', component: PostsComponent },
  { path: 'addPost', component: AddPostComponent },
  { path: 'addPost/:id', component: AddPostComponent },
  {
    path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    SingUpComponent,
    FireStorePostsComponent,
    AddPostComponent,
    LoginComponent,
    AddBookComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatExpansionModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    AngularFireAuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only,
    ),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule


  ],
  providers: [PostsService, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
