import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from './interface/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>

  constructor(public afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router) {
    this.user = this.afAuth.authState;
  }
  // constructor(private http: HttpClient, private db: AngularFirestore) { }
  userCollection: AngularFirestoreCollection = this.db.collection('users');

  signup(email: string, password: string) {
    this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(({ user }) => {
      this.userCollection.doc(user.uid).set({ name: user.email, id: user.uid });
      this.router.navigate(['/books']);
    })
  }

  login(email: string, password: string) {
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['/books']);
      })
  }
  Logout() {
    this.afAuth.auth.signOut().then(res => console.log('successful logout'));
  }
}
