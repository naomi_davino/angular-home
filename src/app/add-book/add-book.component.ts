import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  constructor(public booksService: BooksService, public authService: AuthService,
    public route: ActivatedRoute, public router: Router) { }

  title: string;
  author: string;
  id: string;
  isEdit: boolean = false;
  buttonText: string = "Add Book";
  headerText: string = "Add Book";
  userId: string;

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if (this.id) {
          this.isEdit = true;
          this.buttonText = "Update"
          this.headerText = "Update Book"
          this.booksService.getBook(this.userId, this.id).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;

            }
          )
        }
      }
    )
  }


  onSubmit() {
    if (this.isEdit) {
      this.booksService.updateBook(this.userId, this.id, this.title, this.author);
    }
    else {
      this.booksService.addBook(this.userId, this.title, this.author);
    }
    this.router.navigate(['/books']);
  }

}
