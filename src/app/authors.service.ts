import { Injectable } from '@angular/core';
import {Observable, observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors:any =  [{id:1, author:'sahar'},{id:2, author:'Leo Tolstoy'},{id:3, author:'Chilki bilki'}];
  i:number =3;
  getAuthors(){
    const authorsObservable = new Observable(
      observer =>{
        setInterval (
          ()=> observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
  }

  addAuthors(authornew:string){
    this.i=this.i+1;
    this.authors.push({id:this.i,author:authornew});
  }

  constructor() { }
}
