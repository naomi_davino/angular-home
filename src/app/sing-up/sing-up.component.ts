import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit {

  constructor(public authService:AuthService) { }

username:string;
password:string;

signup(){
  this.authService.signup(this.username,this.password)
}

  ngOnInit() {
  }

}
