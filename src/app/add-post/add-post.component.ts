import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  constructor(public postService: PostsService,
    public router: Router,
    public route: ActivatedRoute,
    public authService: AuthService) { }

  title: string;
  author: string;
  body: string;
  id: string;
  isEdit: boolean = false;
  buttonText: string = "Add POST";
  userId: string;

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if (this.id) {
          this.isEdit = true;
          this.buttonText = "Update"
          this.postService.getPost(this.userId, this.id).subscribe(
            post => {
              this.author = post.data().author;
              this.title = post.data().title;
              this.body = post.data().body;
            }
          )
        }
      }
    )
  }


  onSubmit() {
    if (this.isEdit) {
      this.postService.updatePost(this.userId, this.id, this.title, this.body, this.author);
    }
    else {
      this.postService.addPost(this.userId, this.title, this.body, this.author);
    }
    this.router.navigate(['/posts']);
  }

}
