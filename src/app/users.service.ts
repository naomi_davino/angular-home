import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Users } from './interface/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiurl = "https://jsonplaceholder.typicode.com/users/";

  constructor(private http: HttpClient) { }

  getUser() {
    return this.http.get<Users[]>(this.apiurl);
  }

}
