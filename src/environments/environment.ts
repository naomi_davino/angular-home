// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCtM_RV9SPnipxb1j4_yKKdut3v852JaS8",
    authDomain: "angular-home-5bb27.firebaseapp.com",
    databaseURL: "https://angular-home-5bb27.firebaseio.com",
    projectId: "angular-home-5bb27",
    storageBucket: "angular-home-5bb27.appspot.com",
    messagingSenderId: "753899327950",
    appId: "1:753899327950:web:59d5adfaaacb485860db9f",
    measurementId: "G-V5V3PJCHZM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
